//
//  InterfaceController.swift
//  Watch Test WatchKit Extension
//
//  Created by Simon Theiß on 28/05/15.
//  Copyright (c) 2015 Simon Theiß. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    @IBOutlet weak var `switch`: WKInterfaceSwitch!
    @IBAction func switchAction(value: Bool) {
        NSLog("\(value)")
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
