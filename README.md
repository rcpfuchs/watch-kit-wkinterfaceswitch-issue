# README #

Download this project and run on your apple watch. 

Go to "InterfaceController.swift" in "Watch Test WatchKit Extension" and set a breakpoint to


```
#!swift

    @IBAction func switchAction(value: Bool) {
        NSLog("\(value)")
    }
```

On my watch the value is always false. In simulator value switches between true and false.